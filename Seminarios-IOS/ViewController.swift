//
//  ViewController.swift
//  Seminarios-IOS
//
//  Created by Development on 12/7/21.
//  Copyright © 2021 Development. All rights reserved.
//

import UIKit
import Alamofire
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func doLogin(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let homeViewController = storyboard?.instantiateViewController(withIdentifier: "home") as! HomeViewController
        
        self.present(homeViewController, animated: true, completion: nil)
    }
    
}

